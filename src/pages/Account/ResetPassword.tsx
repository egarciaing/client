import React, { useEffect } from 'react';
import clsx from 'clsx';
import { Button, Container, Typography } from '@material-ui/core';
import { makeStyles, Theme } from '@material-ui/core/styles';
import { useForm } from "react-hook-form";
import { useMutation } from "urql";

import Input from '../../components/Input/Input';
import IAuth from '../../lib/auth/IAuth';
import IPassword from '../../lib/auth/IPassword';

const useStyles = makeStyles((theme: Theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
        display: "contents"
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    margin: {
        margin: theme.spacing(2),
    },
}));

export default function ResetPassword() {
    const classes = useStyles();

    const [response, changePassword] = useMutation<{ auth: IAuth }>(`
        mutation ($password: String!, $newPassword: String!){
            auth: resetPassword(password: $password, token: $token){
                token
            }
        }
    `);

    useEffect(() => {
        if (response.data?.auth.token) {

        }
    }, [response])

    const { control, errors, handleSubmit, getValues } = useForm<IPassword>({
        mode: "onChange",
        defaultValues: {
            password: '',
            confirm: '',
            current: ''
        }
    });

    const onSubmit = (data: IPassword) => changePassword({
        password: data.current,
        newPassword: data.confirm
    });

    return (
        <Container component="main" maxWidth="xs">
            <div className={clsx(classes.paper)} >
                <Typography component="h1" variant="h5"> Reset Password </Typography>
                <form className={clsx(classes.form)} onSubmit={handleSubmit(onSubmit)}>
                    <Input
                        label="New Password"
                        name="password"
                        control={control}
                        error={Boolean(errors.password)}
                        rules={{
                            required: true
                        }}
                        type="password"
                        description={"Type your new password"}
                    >{errors.password && errors.password?.message}</Input>
                    <Input
                        label="Confirm Password"
                        name="confirm"
                        control={control}
                        error={Boolean(errors.confirm)}
                        rules={{
                            required: true,
                            validate(data) {
                                const { password } = getValues();
                                return password.trim() === data.trim() ? true : "Password does not match";
                            }
                        }}
                        type="password"
                        description={"Confirm the new password"}
                    >
                        {errors.confirm && errors.confirm?.message}
                    </Input>
                    <br />
                    <Button
                        type="submit"
                        color="primary"
                        variant="contained"
                        className={clsx(classes.submit)}
                    > Change Password </Button>
                </form>
            </div>
        </Container>
    );
}