import React from 'react';
import { Divider, MenuItem, Typography } from '@material-ui/core';
import {
    Dashboard as DashboardIcon,
    AccountCircleOutlined,
    ExitToAppOutlined
} from "@material-ui/icons"
import { Route, Switch } from 'react-router-dom';

import Title from '../components/Title';
import Wrapper from '../components/Layout/Wrapper';
import ButtonLink from '../components/Link/ButtonLink';
import MenuLink from '../components/Link/MenuLink';
import { useAuth } from "../lib/auth";

import UserProfile from "./Module/User/UserProfile";
import TrainingThemes from './Module/TrainingTheme/TrainingThemes';
import HandleTrainingTheme from './Module/TrainingTheme/HandleTrainingTheme';
import HandleLesson from './Module/Lesson/HandleLesson';
import config from "../config";

export default function Dashboard() {
    const { logout, user } = useAuth();

    return (
        <Switch>
            <Wrapper
                title={<Title>{config.title}</Title>}
                mainListItems={
                    <>
                        <ButtonLink variant="text" color="default" startIcon={<DashboardIcon />} to={"/"} disableElevation>
                            Training Themes
                        </ButtonLink>
                    </>
                }

                profileListItems={(handleMenuClose) => (
                    <div>
                        <MenuItem disabled>
                            <Typography>{user?.email}</Typography>
                        </MenuItem>
                        <Divider />
                        <MenuLink to={"/account"}>

                            <AccountCircleOutlined />
                            <Typography variant="inherit">Profile</Typography>

                        </MenuLink>
                        <MenuItem onClick={() => {
                            handleMenuClose();
                            logout();
                        }}>
                            <ExitToAppOutlined />
                            <Typography variant="inherit">Sign Out</Typography>
                        </MenuItem>
                    </div>
                )}
            >
                <>
                    <Route path={"/"} exact>
                        <TrainingThemes />
                    </Route>
                    <Route path={"/new-training-theme/"} >
                        <HandleTrainingTheme />
                    </Route>
                    <Route path={"/training-theme/:themeId/"} exact>
                        <HandleTrainingTheme />
                    </Route>
                    <Route path={"/training-theme/:themeId/new-lesson"}>
                        <HandleLesson />
                    </Route>
                    <Route path={"/training-theme/:themeId/lesson/:lessonId"}>
                        <HandleLesson />
                    </Route>

                    <Route path={"/account"}>
                        <UserProfile />
                    </Route>
                </>
            </Wrapper>
        </Switch>
    );
}