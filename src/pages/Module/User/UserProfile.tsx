import React, { useEffect } from 'react';
import clsx from 'clsx';
import { Button, Container, Divider, Grid, Typography } from '@material-ui/core';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { useForm } from "react-hook-form";
import { useMutation } from "urql";

import { useAuth, useProfile } from '../../../lib/auth';
import IUser from '../../../lib/auth/IUser';
import Title from '../../../components/Title';
import Input from '../../../components/Input/Input';
import IAuth from '../../../lib/auth/IAuth';
import IPassword from '../../../lib/auth/IPassword';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        margin: {
            margin: theme.spacing(2),
        },
        form: {
            marginTop: theme.spacing(3),
        }
    }),
);


function Security() {
    const classes = useStyles();
    const { logout } = useAuth();
    const [response, changePassword] = useMutation<{ auth: IAuth }>(`
        mutation ($password: String!, $newPassword: String!){
            auth: changePassword(password: $password, newPassword: $newPassword){
                token
            }
        }
    `);

    useEffect(() => {
        if (response.data?.auth.token) {
            logout();
        }
    }, [logout, response])

    const { control, errors, handleSubmit, getValues } = useForm<IPassword>({
        mode: "onChange",
        defaultValues: {
            password: '',
            confirm: '',
            current: ''
        }
    });

    const onSubmit = (data: IPassword) => changePassword({
        password: data.current,
        newPassword: data.confirm
    });

    return (
        <form className={clsx(classes.form)} onSubmit={handleSubmit(onSubmit)}>
            <Grid container>
                <Grid item sm={6} xs={12}>
                    <Input
                        label="New Password"
                        name="password"
                        control={control}
                        error={Boolean(errors.password)}
                        rules={{
                            required: true
                        }}
                        type="password"
                        description={"Type your new password"}
                    >{errors.password && errors.password?.message}</Input>
                </Grid>
                <Grid item sm={6} xs={12}>
                    <Input
                        label="Confirm Password"
                        name="confirm"
                        control={control}
                        error={Boolean(errors.confirm)}
                        rules={{
                            required: true,
                            validate(data) {
                                const { password } = getValues();
                                return password.trim() === data.trim() ? true : "Password does not match";
                            }
                        }}
                        type="password"
                        description={"Confirm the new password"}
                    >
                        {errors.confirm && errors.confirm?.message}
                    </Input>
                </Grid>
                <Grid item sm={6} xs={12}>
                    <Input
                        label="Current Password"
                        name="current"
                        control={control}
                        rules={{
                            required: true
                        }}
                        type="password"
                        description={"Your current password"}
                    ></Input>
                </Grid>
            </Grid>
            <br />
            <Button
                type="submit"
                color="primary"
                variant="contained"
                className={clsx(classes.margin)}
            >
                Change Password
        </Button>
        </form>
    );
}

export default function Profile() {
    const [profile, fetching] = useProfile({ pause: false });
    const { logout } = useAuth();
    const classes = useStyles();

    const { control, errors, handleSubmit } = useForm<IUser>({
        mode: "onChange",
        defaultValues: profile || undefined
    });

    const [response, changeEmail] = useMutation<{ auth: IAuth }>(`
        mutation ($email: String!){
            auth: changeEmail(email: $email){
                token
            }
        }
    `);

    useEffect(() => {
        if (response.data?.auth.token) {
            logout();
        }
    }, [logout, response])

    if (!profile) {
        return <span>loading</span>;
    }

    const onSubmit = (data: IUser) => changeEmail({
        email: data.email
    });

    return (
        <Container maxWidth="md">
            <Title>Account</Title>
            <Typography variant="subtitle2" color="textSecondary">Profile</Typography>
            <Divider />
            {!fetching && (
                <form onSubmit={handleSubmit(onSubmit)}>
                    <Input
                        label="Email"
                        name="email"
                        description={"The user email"}
                        control={control}
                        error={Boolean(errors.email)}
                        rules={{
                            required: true,
                            pattern: {
                                message: "Invalid format email",
                                value: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/
                            }
                        }}
                    >{errors.email && errors.email?.message}</Input>
                    <br />
                    {response.error?.message}
                    <Button
                        type="submit"
                        color="primary"
                        variant="contained"
                        className={clsx(classes.margin)}
                    >
                        Update Profile
                </Button>
                </form>
            )}
            <Typography variant="subtitle2" color="textSecondary">Security</Typography>
            <Divider />
            <Security />
        </Container>
    )
}