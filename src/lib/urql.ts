import { createClient, dedupExchange } from 'urql';
import { multipartFetchExchange } from '@urql/exchange-multipart-fetch';
import { cacheExchange } from '@urql/exchange-graphcache';
import { authExchange } from '@urql/exchange-auth';
import {
    addAuthToOperation,
    didAuthError,
    getAuth,
    willAuthError
} from "./auth/strategy";

import config from "../config";

export const client = createClient({
    url: config.endpoint,
    exchanges: [
        dedupExchange,
        cacheExchange({}),
        authExchange({
            addAuthToOperation,
            willAuthError,
            didAuthError,
            getAuth
        }),
        multipartFetchExchange,
    ],
    preferGetMethod: true
});