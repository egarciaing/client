export const wrapImage: (data: string) => Promise<HTMLImageElement> = (data: string) => new Promise((res) => {
    const img = new Image();
    img.onload = function () {
        res(img);
    }
    img.crossOrigin = 'Anonymous';
    img.src = data;
});

export function readImage(file: File): Promise<HTMLImageElement> {
    return readFile(file).then((dataUrl) => wrapImage(dataUrl as string))
}

/**
 * load file as promise
 */
export default function readFile(file: File, asBuffer: boolean = false): Promise<string | ArrayBuffer | null> {
    const reader = new FileReader();

    if (!file) {
        return Promise.resolve(null);
    }

    return new Promise((res, rej) => {
        reader.onload = () => {
            res(reader.result);
        };
        reader.onerror = () => {
            rej(reader.error);
        };

        if (asBuffer) {
            reader.readAsArrayBuffer(file);
        } else {
            reader.readAsDataURL(file);
        }
    })
};