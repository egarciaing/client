import { useState, useEffect } from "react";
import { wrapImage } from "./readFile";
const ColorThief = require("color-thief");

type RGB = "rgb";
type Raw = "matrix";
type Format = RGB | Raw;

interface ColorProps<T> {
    quality?: number;
    image?: HTMLImageElement;
    url?: string;
    format: T;
}

export function useColor<F extends Format, T = F extends RGB ? string : number[]>({ image, url, format, quality = 10 }: ColorProps<F>): T | undefined {
    const [value, setColor] = useState<T | undefined>();

    useEffect(() => {
        const img = image ? image : (url && wrapImage(url));
        if (img) {
            const colorized = new ColorThief();
            const color = colorized.getColor(img, quality);

            setColor(format === "matrix" ? color : `rgb(${color.join(",")})`);
        }
    }, [image, url, quality, format]);

    return value;
}

export function useColors<F extends Format, T = F extends RGB ? string[] : number[][]>({ image, url, format, quality = 6, palette = 4 }: ColorProps<F> & { palette?: number }): T | undefined {
    const [values, setColors] = useState<T | undefined>();

    useEffect(() => {
        const img = image ? image : (url && wrapImage(url));
        if (img) {
            const colorized = new ColorThief();
            const colors = colorized.getPalette(img, palette, quality);

            setColors(format === "matrix" ? colors : colors.map((color: number[]) => `rgb(${color.join(",")})`));
        }
    }, [image, url, quality, format, palette]);

    return values;
}