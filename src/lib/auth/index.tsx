import React, { createContext, ReactElement, ReactNode, useContext, useEffect, useState } from "react";
import { useMutation, useQuery, OperationContext } from "urql";

import { removeToken, storeToken, tokenStractor } from "./store";
import { AuthContext } from "./IContext";
import IAuth from "./IAuth";
import IUser from "./IUser";

interface ProviderProps {
    children: ReactNode | ReactElement
};

const authContext = createContext<AuthContext>({
    fetching: false,
    login: () => Promise.reject("Not yet implemented"),
    logout() {
        throw new Error("Not yet implemented");
    }
});
authContext.displayName = "AuthContext";

export function useLogin() {
    return useMutation<{ auth: IAuth }>(`
        mutation ($email:String!, $password:String!){
            auth: login(email:$email, password:$password) {
                user { 
                    email
                    id
                }
                token
            }
        }
    `);
}

export function useToken(): string | null {
    const { token } = useContext(authContext);
    return token || null;
}

export function useAuth() {
    return useContext(authContext);
}

export function useProfile(param?: { pause: boolean }): [IUser | null, boolean, (opts?: Partial<OperationContext>) => void] {
    const [respose, refresh] = useQuery<{ auth: Partial<IAuth> | null }>({
        pause: param?.pause,
        query: (`
        query {
            auth: me { 
                user {
                    email,
                    id
                }
                token
            }
        }`)
    });

    return [respose.data?.auth?.user || null, respose.fetching, refresh];
}

export function AuthProvider({ children }: ProviderProps) {
    const initialState = { token: tokenStractor(false), user: null };
    const [auth, setData] = useState<IAuth>(initialState);
    const [loading, isLoading] = useState<boolean>(true);
    const [profile, fetching] = useProfile();
    const [response, login] = useLogin();
    
    useEffect(() => {
        const newToken: string | null = response.data?.auth?.token || null;

        if (newToken) {
            storeToken(newToken);
        }

        const storedToken = tokenStractor(false);
        const data = response.data?.auth || {
            user: profile,
            token: storedToken
        };

        setData(data);
        isLoading(fetching || response.fetching);
    }, [response, profile, fetching]);

    const logout = () => {
        removeToken();
        setData(initialState);
    };

    return (
        <authContext.Provider
            value={{
                user: auth.user,
                token: auth.token,
                error: response.error,
                login: login as any,
                fetching: (loading),
                logout
            }}
        >
            {children}
        </authContext.Provider>
    )
}