import IUser from "./IUser";

export default interface IAuth<U extends IUser = IUser> {
    user?: U | null;
    token?: string | null;
}
