import { CombinedError, OperationResult } from "urql";
import IAuth from "./IAuth";
import IUser from "./IUser";


export interface TokenContext {
    token: string | null
    setToken(token: string | null): void
}

export interface AuthContext extends IAuth {
    fetching: boolean,
    error?: CombinedError | null;
    login(auth: { email: string, password: string }): Promise<OperationResult<IAuth<IUser>>>;
    logout(): void;
}

export interface ProfileContext {
    setProfile(profile: IUser | null): void;
}