export interface IPaginate {
    offset: number;
    limit: number;
}

export interface RenderElement<D> {
    (data: D, index: number): React.ReactElement | React.ReactNode | null;
}

export default interface IPagination<T> {
    data: Array<T>;
    limit: number;
    page: number;
    onChange(nextPage: number, limit: number): void;
} 