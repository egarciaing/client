import React from 'react';
import { Route, BrowserRouter as Router, Switch, Redirect } from 'react-router-dom';

import ProtectedRoute from './components/ProtectedRoute';
import { useAuth } from "./lib/auth";

import './App.css';
import SignIn from './pages/Account/SignIn';
import Dashboard from './pages/Dashboard';
import ForgotPassword from './pages/Account/ForgotPassword';
import ResetPassword from './pages/Account/ResetPassword';

function App() {
  const { user } = useAuth();
  const signin = Boolean(user);

  return (
    <Router>
      <div>
        {signin ?? <Redirect to={"/login"} />}
        <Switch>
          <Route path={"/login"}>
            <SignIn />
          </Route>

          <Route path={"/forgot-password"}>
            <ForgotPassword />
          </Route>

          <Route path={"/reset-password/:token"}>
            <ResetPassword />
          </Route>

          <ProtectedRoute path={"/"} fallback={"/login"}>
            <Dashboard />
          </ProtectedRoute>

          <Route path="*"> Not Found </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
