import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Typography } from '@material-ui/core';

interface AlertDeleteProps {
    onClose(isOk:boolean): void;
    open: boolean;
}
export default function AlertDelete({ open, onClose }: AlertDeleteProps) {

    return (
        <div>
            <Dialog
                open={open}
                onClose={onClose}
                aria-labelledby="delete-dialog-title"
                aria-describedby="delete-dialog-description"
            >
                <DialogTitle id="delete-dialog-title">{"Are you sure you want to delete this resource?"}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="delete-dialog-description">
                        It is possible that this action is irreversible,
                        if you are sure to continue press the <Typography gutterBottom color="error" component="span" >DELETE</Typography> button
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={()=> onClose(false)} color="primary"> Cancel </Button>
                    <Button onClick={()=> onClose(true)} variant="outlined" autoFocus> Delete </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
