import React from "react"
import { Link, LinkProps } from "react-router-dom";
import Button, { ButtonProps } from "@material-ui/core/Button";

type ButtonLinkProps = Pick<ButtonProps, "color" | "variant" | "disabled" | "className" | "title" | "style" | "startIcon" | "endIcon" | "disableElevation"> &
    Pick<LinkProps, "to" | "replace" | "children">;

function ButtonLink({ children, to, replace, ...props }: ButtonLinkProps) {
    return (
        <Button component={Link} to={to} replace={replace} {...props} >
            {children}
        </Button>
    )
}

export default ButtonLink;