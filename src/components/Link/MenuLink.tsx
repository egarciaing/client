import React from "react"
import { Link, LinkProps } from "react-router-dom";
import MenuItem, { MenuItemProps } from "@material-ui/core/MenuItem";

type ButtonLinkProps = Pick<MenuItemProps, "color" | "alignItems" | "disableGutters" | "radioGroup" | "title" | "style"> &
    Pick<LinkProps, "to" | "replace" | "children" | "className">;

function MenuLink({ children, to, replace, ...props }: ButtonLinkProps) {
    return (
        <MenuItem component={Link} to={to} replace={replace} {...props} >
            {children}
        </MenuItem>
    )
}

export default MenuLink;