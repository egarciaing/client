import React from "react";
import { Route, Redirect } from "react-router-dom";
import { useAuth } from "../lib/auth";

interface ProtectedRouteProps extends Record<string, any> {
    children: React.ReactElement | React.ReactNode;
    fallback: string;
}

export default function ProtectedRoute({ children, fallback, ...rest }: ProtectedRouteProps): React.ReactElement {
    const { fetching, user } = useAuth();
    
    return (<Route
        {...rest}
        render={({ location }) => {
            return fetching ? <span>Loading...</span> : (
                user ? (children) : (
                    <Redirect
                        to={{
                            pathname: fallback,
                            state: { from: location }
                        }}
                    />
                )
            )
        }}
    />);
}