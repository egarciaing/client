import Area from "./Area";
import Input from "./Input";
import FileInput from "./FileInput";
import InputBase from "./InputBase";
import Select from "./Select";

export * from "./InputBase";

export {
    Area, Input, FileInput, InputBase, Select
};

export default Input;