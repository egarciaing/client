import React from 'react';
import { TextField } from '@material-ui/core';
import { Controller } from "react-hook-form";
import InputBase from "./InputBase";
import Input, { InputProps } from "./Input";

function Area({ label, name, children, description, readOnly, control, error, orientation, ...rest }: InputProps, ref: any) {

    return (
        <InputBase
            name={name}
            label={label}
            description={description}
            error={error && children}
            orientation={orientation}
        >
            <Controller
                as={<TextField ref={ref} />}
                id={name}
                name={name}
                control={control}
                variant="outlined"
                multiline
                inputProps={{ 'aria-label': name }}
                {...rest}
            />
        </InputBase>
    )
}

const ForwadedInput = React.forwardRef(Area);
ForwadedInput.displayName = "Area";
ForwadedInput.propTypes = Input.propTypes;

export default ForwadedInput;