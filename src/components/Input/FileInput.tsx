import React, { forwardRef } from 'react';
import omit from "lodash/omit";
import PropTypes from 'prop-types';
import { Button, FormControl, FormHelperText, PropTypes as PT } from "@material-ui/core";
import { PhotoCamera } from "@material-ui/icons";
import InputBase, { InputBaseProps } from './InputBase';

interface FileInputProps extends Omit<InputBaseProps, "orientation"> {
    accept?: string;
    endIcon?: React.ReactNode;
    variant?: 'text' | 'outlined' | 'contained';
    color?: PT.Color;
    multiple?: boolean;
}

function FileInput(props: FileInputProps, ref: any) {
    const { label, error, description, variant, endIcon, color, children, ...raw } = props;

    const describedby = `file-${raw.name}-helper-text`;
    const id = `${raw.name}-button-file`;

    return (
        <>
            <FormControl error={Boolean(error)} size="small" variant="outlined">
                <div>
                    <input id={id} ref={ref} type="file" {...raw} hidden />
                    <label htmlFor={id}>
                        <Button
                            color={color}
                            aria-label="upload file"
                            variant={variant}
                            component="span"
                            startIcon={<PhotoCamera />}
                            endIcon={endIcon}
                        >
                            {label}
                        </Button>
                    </label>
                </div>
                {children && <FormHelperText >{children}</FormHelperText>}
                {description && <FormHelperText id={describedby} >{description}</FormHelperText>}
            </FormControl>
        </>
    )
}

const ForwadedFileInput = forwardRef(FileInput);
ForwadedFileInput.displayName = "FileInput";
ForwadedFileInput.propTypes = {
    ...omit(InputBase.propTypes, "orientation"),
    accept: PropTypes.string
}

export default ForwadedFileInput;