import React, { useMemo } from "react";
import PropTypes from "prop-types";
import { Select as MuiSelect, MenuItem, } from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { useBorderSelectStyles } from "@mui-treasury/styles/select/border";

import { Controller } from "react-hook-form";
import InputBase from "./InputBase";
import Input, { InputProps } from "./Input";


interface SelectPorps extends InputProps {
    options: Array<{ label: string, value: any }>
}
function Select({ label, name, children, description, readOnly, control, error, options, ...rest }: SelectPorps, ref: any) {

    const borderSelectClasses = useBorderSelectStyles();

    const items = useMemo(() => {
        return options.map(item => <MenuItem value={item.value} key={item.label}>{item.label}</MenuItem>)
    }, [options])

    const ExpandIcon = (props: any) => {
        return (
            <ExpandMoreIcon className={props.className + " " + borderSelectClasses.icon} />
        )
    };

    const menuProps = {
        classes: {
            list: borderSelectClasses.list
        },
        anchorOrigin: {
            vertical: "bottom",
            horizontal: "left"
        },
        transformOrigin: {
            vertical: "top",
            horizontal: "left"
        },
        getContentAnchorEl: null
    };

    return (
        <InputBase
            name={name}
            label={label}
            description={description}
            error={error && children}
        >
            <Controller
                as={<MuiSelect ref={ref} children={items} />}
                id={name}
                name={name}
                control={control}
                classes={{ root: borderSelectClasses.select, }}
                labelId="inputLabel"
                IconComponent={ExpandIcon}
                MenuProps={menuProps}
                {...rest}
            />
        </InputBase>
    )
}

const ForwadedInput = React.forwardRef(Select);
ForwadedInput.displayName = "Select";
ForwadedInput.propTypes = {
    ...Input.propTypes,
    options: PropTypes.array.isRequired
};

export default ForwadedInput;