import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { FormControl, FormHelperText, Typography, Grid } from '@material-ui/core';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            maxWidth: "60ch"
        },
        label: {
            textAlign: "right",
            margin: theme.spacing(1.5, 1, 0, 1),
            [theme.breakpoints.down("xs")]: {
                textAlign: "left"
            }
        },
        labelLeft: {
            textAlign: "left"
        },
        margin: {
            margin: theme.spacing(1, 0, 1, 0),
        },
        textField: {
            width: '100%',
        },
    }),
);

type Orientation = "vertical" | "horizontal";

export interface InputBaseProps {
    name: string;
    label: string;
    error?: React.ReactNode;
    children?: string | React.ReactNode;
    description?: string | React.ReactNode;
    orientation?: Orientation;
}

function InputBase({ label, name, children, description, error, orientation = "horizontal" }: InputBaseProps) {
    const classes = useStyles();
    const describedby = `outlined-${name}-helper-text`;

    return (
        <Grid container spacing={0} className={clsx(classes.root)}>
            <Grid item sm={orientation === "horizontal" ? 4 : undefined} xs={12}>
                <label htmlFor={name} >
                    <Typography variant="subtitle1" className={clsx(classes.label, orientation === "vertical" && classes.labelLeft)}>
                        <strong>{label}</strong>
                    </Typography>
                </label>
            </Grid>
            <Grid item sm={orientation === "horizontal" ? 8 : undefined} xs={12}>
                <FormControl error={Boolean(error)} className={clsx(classes.margin, classes.textField)} size="small" variant="outlined">
                    {children}
                    {Boolean(error) && <FormHelperText >{error}</FormHelperText>}
                    {Boolean(description) && <FormHelperText id={describedby} >{description}</FormHelperText>}
                </FormControl>
            </Grid>
        </Grid>
    )
}

InputBase.propTypes = {
    label: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    children: PropTypes.node,
    description: PropTypes.node,
    error: PropTypes.node,
    orientation: PropTypes.oneOf(["vertical", "horizontal"])
};

export default InputBase;