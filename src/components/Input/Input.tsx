import React from 'react';
import omit from "lodash/omit";
import PropTypes from 'prop-types';
import { OutlinedInput } from '@material-ui/core';
import { Controller, Control, ValidationRules } from "react-hook-form";
import InputBase, { InputBaseProps } from "./InputBase";

export interface InputProps extends InputBaseProps {
    control: Control;
    readOnly?: boolean;
    children?: string | React.ReactNode;
    description?: string | React.ReactNode;
    rules?: ValidationRules;
    type?: string;
}

function Input({ label, name, children, description, readOnly, control, error, orientation, ...rest }: InputProps, ref: any) {
    return (
        <InputBase
            name={name}
            label={label}
            description={description}
            error={error && children}
            orientation={orientation}
        >
            <Controller
                as={<OutlinedInput ref={ref} />}
                id={name}
                name={name}
                inputProps={{ 'aria-label': name }}
                control={control}
                readOnly={readOnly}
                {...rest}
            />
        </InputBase>
    )
}

const ForwadedInput = React.forwardRef(Input);
ForwadedInput.displayName = "Input";
ForwadedInput.propTypes = {
    ...omit(InputBase.propTypes, "orientation"),
    readOnly: PropTypes.bool,
    control: PropTypes.any.isRequired,
    rules: PropTypes.object,
    type: PropTypes.string
};

export default ForwadedInput;