import React from "react";
import clsx from "clsx";
import { Box, Grid, Typography, CircularProgress, Card, CardActionArea, CardContent } from "@material-ui/core";
import { makeStyles, createStyles } from '@material-ui/core/styles';
import InfiniteScroll from "react-infinite-scroller";
import { Add as AddIcon } from '@material-ui/icons';

import IPagination, { RenderElement } from "../../lib/IPagination";
import { ID } from "../../lib/ID";

interface GridListProps<T> extends Omit<IPagination<T>, "page"> {
    title: JSX.Element | string | null;
    subtitle?: string;
    renderItem: RenderElement<T>;
    hasMore: boolean;
    useWindow?: boolean;
    threshold?: number;
}

interface AddNewItemProps {
    onClick?(id: ID): void;
    label: string;
}

export const useItemStyles = makeStyles((theme) =>
    createStyles({
        root: {
            maxWidth: 345,
            minHeight: theme.spacing(20)
        },
        optionIcon: {
            marginLeft: "auto"
        },
        content: {
            minHeight: theme.spacing(23)
        },
        contentFlex: {
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
        }
    })
);

export function AddNewItem({ onClick, label }: AddNewItemProps) {
    const classes = useItemStyles();
    return (
        <Card className={classes.root}>
            <CardActionArea onClick={() => onClick && onClick(0)}>
                <CardContent className={clsx(classes.content, classes.contentFlex)}>
                    <AddIcon fontSize="large" color="primary" />
                    <Typography color="primary" >{label}</Typography>
                </CardContent>
            </CardActionArea>
        </Card>
    );
}

export default function GridList<T = any>({ data, renderItem, onChange, title, subtitle, limit, ...rest }: GridListProps<T>) {
    const bodyGrid = data.map((item, index) => {
        const rItem = renderItem(item, index);
        return rItem && <Grid item lg={3} md={4} sm={6} xs={12} key={index}> {rItem} </Grid>
    });

    return (
        <Box>
            {title}
            {Boolean(subtitle) && <Typography variant="subtitle1" color="textSecondary">{subtitle}</Typography>}
            <InfiniteScroll
                loader={<CircularProgress color="inherit" />}
                loadMore={(nextPage) => onChange(nextPage, limit)}
                {...rest}
            >
                <Grid container direction="row" justify="flex-start" alignItems="flex-start" spacing={3}>
                    {bodyGrid}
                </Grid>
            </InfiniteScroll>
        </Box>
    );
}