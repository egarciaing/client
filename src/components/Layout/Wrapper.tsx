import React, { useMemo, useRef } from 'react';
import clsx from 'clsx';
import PropTypes from "prop-types"
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import Box from '@material-ui/core/Box';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Container from '@material-ui/core/Container';
import MenuIcon from '@material-ui/icons/Menu';
import CloseIcon from '@material-ui/icons/Close';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import Menu from '@material-ui/core/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Copyright from '../Copyright';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    toolbar: {
        paddingRight: 24, // keep right padding when drawer closed
    },
    toolbarIcon: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    toolbarTitle: {
        paddingRight: theme.spacing(2)
    },
    toolbarContent: {
        flexGrow: 1,
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    menuButtonHidden: {
        display: 'none',
    },
    drawerPaper: {
        position: 'relative',
        whiteSpace: 'nowrap',
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerPaperClose: {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        width: theme.spacing(7),
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9),
        },
    },
    appBarSpacer: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
    },
    container: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
        //backgroundColor: "whitesmoke"
    },
    paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
    fixedHeight: {
        height: 240,
    },
}));

interface SideMenuProps {
    open: boolean;
    title?: React.ReactNode;
    list: React.ReactNode;
    onClose(): void;
}

function SideMenu({ open, title, list, onClose }: SideMenuProps) {
    const classes = useStyles();

    return (
        <Drawer
            variant="permanent"
            classes={{
                paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
            }}
            open={open}
        >
            <div className={classes.toolbarIcon}>
                {title}
                <IconButton onClick={onClose}>
                    <ChevronLeftIcon />
                </IconButton>
            </div>
            <Divider />
            <List>{list}</List>
        </Drawer>
    )
}

interface WrapperProps {
    children: React.ReactElement;
    mainListItems: React.ReactElement | React.ReactNode;
    profileListItems: (handleMenuClose: () => void) => React.ReactElement | React.ReactNode | null;
    useDrawer?: boolean
    title?: React.ReactNode | string;
}

function Wrapper({ children, title, mainListItems, useDrawer, profileListItems }: WrapperProps) {
    const classes = useStyles();
    const [open, setOpen] = React.useState(true);
    const [isShowProfile, showProfile] = React.useState<boolean>(false);

    const anchorEl = useRef<HTMLButtonElement | null>(null);
    const menuId = 'primary-search-account-menu';

    const TitleComponent = useMemo(() => {
        return (
            <Box className={clsx(classes.toolbarTitle, useDrawer && classes.toolbarContent)}>
                {(!open || !useDrawer) && title}
            </Box>
        )
    }, [classes, title, useDrawer, open]);

    const handleDrawerOpen = () => {
        setOpen(true);
    };
    const handleDrawerClose = () => {
        setOpen(false);
    };

    const handleProfileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
        showProfile(true);
    };
    const handleProfileMenuClose = () => {
        showProfile(false);
    };

    const profileItems = profileListItems(handleProfileMenuClose);
    const showProfileButton = Boolean(profileItems);

    const profileMenu = (
        <Menu
            anchorEl={anchorEl.current}
            anchorOrigin={{ vertical: "top", horizontal: 'right' }}
            id={menuId}
            keepMounted
            transformOrigin={{ vertical: "top", horizontal: 'right' }}
            open={isShowProfile}
            onClose={handleProfileMenuClose}
        >
            {profileItems}
        </Menu>
    );


    return (
        <div className={classes.root}>
            <CssBaseline />
            <AppBar position="absolute" color="inherit" className={clsx(useDrawer && classes.appBar, useDrawer && open && classes.appBarShift)}>
                <Toolbar className={clsx(useDrawer && classes.toolbar)}>
                    {useDrawer &&
                        <IconButton
                            edge="start"
                            color="inherit"
                            aria-label="open drawer"
                            onClick={handleDrawerOpen}
                            className={clsx(classes.menuButton, open && classes.menuButtonHidden)}
                        >
                            {open ? <CloseIcon /> : <MenuIcon />}
                        </IconButton>
                    }

                    {TitleComponent}
                    {!useDrawer &&
                        <Box className={classes.toolbarContent} >
                            {mainListItems}
                        </Box>
                    }

                    <Box>
                        {showProfileButton && (
                            <IconButton
                                ref={anchorEl}
                                aria-label="Account of current user"
                                aria-controls="menu-appbar"
                                aria-haspopup="true"
                                onClick={handleProfileMenuOpen}
                                color="inherit"
                            >
                                <AccountCircle />
                            </IconButton>
                        )}
                    </Box>
                </Toolbar>
            </AppBar>

            {useDrawer &&
                <SideMenu
                    list={mainListItems}
                    onClose={handleDrawerClose}
                    open={open}
                    title={title}
                />
            }

            <main className={classes.content}>
                <div className={classes.appBarSpacer} />
                <Container maxWidth="lg" className={classes.container}>
                    {children}
                    <Box pt={4}>
                        <Copyright />
                    </Box>
                </Container>
            </main>
            {profileMenu}
        </div>
    );
}


Wrapper.propTypes = {
    children: PropTypes.node.isRequired,
    title: PropTypes.node,
    mainListItems: PropTypes.node,
    profileListItems: PropTypes.func,
    useDrawer: PropTypes.bool
}
Wrapper.defaultProps = {
    useDrawer: false
}
export default Wrapper;