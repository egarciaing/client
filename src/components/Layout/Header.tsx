import React from "react";
import { Box } from "@material-ui/core";
import { makeStyles, createStyles } from '@material-ui/core/styles';
import { useColor } from "../../lib/useColor";

export const useHeaderStyle = makeStyles((theme) =>
    createStyles({
        root: {
            minHeight: theme.spacing(40),
            width: `calc(100% + ${theme.spacing(6)}px)`,
            display: "inline-block",
            paddingBottom: theme.spacing(4),
            backgroundColor: theme.palette.primary.main,
            margin: theme.spacing(-4, 0, 0, -3),
            paddingLeft: theme.spacing(4),
            paddingRight: theme.spacing(4),
            overflow: "hidden",
            [theme.breakpoints.down("sm")]: {
                paddingLeft: 0,
                paddingRight: 0,
            }
        },
        image: {
            backgroundPosition: "center center",
            backgroundAttachment: "scroll",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
            height: theme.spacing(30),
            width: "90%",
            display: "block",
            margin: "auto",
            [theme.breakpoints.down("sm")]: {
                width: "100%",
                height: theme.spacing(40),
            }
        }
    })
);

export interface HeaderProps {
    image?: HTMLImageElement;
    children?: React.ReactNode;
}

export default function Header({ image, children }: HeaderProps) {
    const classes = useHeaderStyle();
    const thecolor = useColor({ image, format:"rgb" });

    return (
        <Box className={classes.root} style={{ backgroundColor: thecolor }}>
            {children}
            {image &&
                <Box
                    className={classes.image}
                    style={{
                        backgroundImage: `url(${image.src})`,
                    }}
                />
            }
        </Box>
    );
}